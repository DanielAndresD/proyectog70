from django.contrib import admin
from .models.categoria import Categoria
from .models.productos import Productos
from .models.account import Account
from .models.user import User


admin.site.register(Categoria)
admin.site.register(Productos)
admin.site.register(User)
admin.site.register(Account)





