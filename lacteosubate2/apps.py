from django.apps import AppConfig


class Lacteosubate2Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lacteosubate2'
