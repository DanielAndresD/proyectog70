from django.db import models
from django.db.models.base import Model
from django.db.models.fields.related import ForeignKey


class Categoria(models.Model):
    idCategoria  = models.BigAutoField(primary_key=True)
    nombreCategoria = models.CharField('nombreCategoria', max_length = 15, unique=True)
    descripcionCategoria = models.CharField('descripcionCategoria', max_length = 300, unique=False)

    def __str__(self):
        return self.nombreCategoria

  #  def __init__ (self,idCategoria,nombreCategoria,descripcionCategoria):
  #    self.idCategoria = idCategoria
  #    self.nombreCategoria = nombreCategoria
  #    self.descripcionCategoria = ""

  