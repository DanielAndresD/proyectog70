from django.db import models
from django.db.models.base import Model
from django.db.models.fields.related import ForeignKey
from .categoria import Categoria

#POO con Python
#Declarar una clase en python
class Productos(models.Model):
    idProducto= models.BigAutoField(primary_key=True)
    nombreProducto= models.CharField('nombreProducto',max_length=30, unique=False)
    precioProducto= models.IntegerField(default=0)
    #idCategoriaProducto=models.ForeignKey(Categoria,on_delete=models.CASCADE) # debemos esperar la clase categoria para la llave foranea    unidadProducto=models.CharField('unidadProducto',max_length=30, unique=False)
    # idCategoriaProducto=models.ForeignKey(Categoria,related_name='idCategoriaProducto',on_delete=models.CASCADE) # debemos esperar la clase categoria para la llave foranea    unidadProducto=models.CharField('unidadProducto',max_length=30, unique=False)
    categoria = models.ForeignKey(Categoria, on_delete=models.SET_NULL,null=True)
    unidadProducto=models.CharField('unidadProducto',max_length=30, unique=False)
    descripcionProducto=models.CharField('descripcionProducto',max_length=300, unique=False)

    def __str__(self):
        return self.nombreProducto
    #Constructor
   # def __init__(self,idProducto,nombreProducto,precioProducto,idCategoriaProducto,unidadProducto,descripcionProducto):#Self es equiv a this en Java
    #    self.idProducto=idProducto
    #    self.nombreProducto=nombreProducto
    #    self.precioProducto= precioProducto
    #    self.idCategoriaProducto=idCategoriaProducto
    #    self.unidadProducto=unidadProducto
    #    self.descripcionProducto=""
        
         #Métodos    

        
        
