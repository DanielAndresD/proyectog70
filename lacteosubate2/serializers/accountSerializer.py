from lacteosubate2.models.account import Account
from rest_framework import serializers

# The ModelSerializer class provides a shortcut that lets you 
# automatically create a Serializer class with fields that correspond 
# to the Model fields.
class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ['lastChangeDate', 'isActive']