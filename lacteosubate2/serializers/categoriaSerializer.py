from lacteosubate2.models.categoria import Categoria
from rest_framework import serializers

class CategoriaSerializer(serializers.ModelSerializer):
    class Meta():
        model = Categoria
        fields = ['idCategoria', 'nombreCategoria', 'descripcionCategoria']

    def create(self, validated_data):
        #categoriaData = validated_data.pop('Categoria')
        categoriaInstance = Categoria.objects.create(**validated_data)
        return categoriaInstance
    
    def to_representation(self, instance):
        return super().to_representation(instance)

    #def to_representation(self, obj):
     #   categoria = Categoria.objects.get(id=obj.idCategoriaProducto)
        
      #  return {
       #         'idCategoriaProducto': Categoria.idCategoriaProducto,
        #        'nombreCategoria': Categoria.nombreCategoria,
         #       'descripcionCategoria': Categoria.descripcionCategoria,
        #}                 


