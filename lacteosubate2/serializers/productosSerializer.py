from lacteosubate2.models.productos import Productos
from rest_framework import serializers


class ProductosSerializer(serializers.ModelSerializer):
    class Meta():
        model=Productos
        fields=['idProducto','nombreProducto','precioProducto','categoria','unidadProducto','descripcionProducto']

    def create(self,validated_data):
        #productosData=validated_data.pop('productos')
        productosInstance=Productos.objects.create(**validated_data)
        return productosInstance
        #Se creó atumatico
    
    def to_representation(self, instance):
        return super().to_representation(instance)
        
        
